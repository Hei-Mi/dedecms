# dedecms官方源码备份

#### 介绍
dedecms是一个强大的开源内容管理系统，是优秀的PHP程序之一。由于官方服务器有时打不开，所以做一个备份，源码无任何修改，原版打包。同步官方发布的程序，保证原汁原味，请支持织梦官方程序，本处只做程序备份，不提供任何服务。官方下载地址如下：
> http://www.dedecms.com/products/dedecms/downloads/
#### 如何下载
在上面的压缩包里面找到你需要的编码，直接点击下载即可；带有full的是指包含所有模块，例如DedeCMS-V5.7-UTF8-SP2-Full.tar.gz指带有所有模块的utf8编码的源程序。


#### 程序相关信息

1. 版本信息：V5.7SP2正式版(2018-01-09)
1. Nginx/IIS/Apache + PHP5/PHP7
1. MySQL4/5 或 SQLite
1. 更新时间：2018年01月09日

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
